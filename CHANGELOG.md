## changelog
    
    3.3.10 allow pass database port from config.js
    3.3.9 update dependcy and fix npm audit warning
    3.3.8 fix @subQuerys and dependencies audit warning
    3.3.7 fix npm audit warnnings
    3.3.6 bugfix and fix batch update api
    3.3.3 add @All and proxy util
    3.3.1 update app config to allow enable/disable database and swagger
    3.3.0 move app initial code to core for reduce verbose code
    3.2.3 clean up package.json and package-lock.json
    3.2.0 change how d.ts is packaged to have better webstorm auto import (both way works in vscode...)
    3.1.2 add HTTP Method PATCH/OPTIONS, reformat code, use ts-mocha for testing(works in both windows and linux)
    3.1.1 upgrade dependencies
    3.1.0 use terser-webpack-plugin instead of UglifyJsPlugin for es6+ support
    3.0.16 change TypeScript compiler target to es2017 to reduce build size
    3.0.15 add a @PagedSQL for customize paged SQL
    3.0.14 put core and corecli in seperate repository
    3.0.13 refactor code structure
    3.0.12 upgrade angular to 7
    3.0.11 use inuqirer for corecli
    3.0.9 add docker to domain
    3.0.8 improve http middleware(callbacks)
    3.0.7 improve /${entity}/pagination, see generated swagger api doc for more information
    3.0.6 upgrade to typescript 3
    3.0.5 upgrade webpack to latest and improve swagger api doc
    3.0.3 expose a logger instead of direct usage of winston
    3.0.1 add @readOnly() @writeOnly() to entity decorator.
    3.0.0 upgrade api-doc to swagger, visit http://localhost:3000/api-doc for api doc with swagger. (use https://editor.swagger.io to edit /domain/swagger.yaml as will)
    2.2.17 upgrade to webpack4
    2.2.16 upgrade to winston3 and initial support for swagger
    2.2.12 update core log to winston.silly and fix debug info is written in error log while working with pm2
    2.2.6 merge default CRUD requests e.g. {Post} /api/users {...} for insert, {Post} /api/users [{...},{...}] for batch insert
    2.2.0 initial support for socket.io
    2.1.5 allow controller level disable auth
    2.1.4 remove rxjs-compat
    2.1.2 use ts-node in webpack
