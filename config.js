/**
 * Created by enixjin on 9/24/15.
 */
var config = {};

config.db = "MySQL";//MySQL or PostgreSQL
config.loginType = "MySQL";

//db account
var db_server = "localhost";
var db_user = "root";
var db_password = "root";

config.distributedDBs = [
    {
        connectionLimit: 10,
        host: db_server,
        database: 'iot',
        user: db_user,
        password: db_password,
        commandPort: 5101,
        socketPort: 5100
    }
];

config.centralizedDB = {
    connectionLimit: 5,
    host: 'localhost',
    database: 'iot',
    user: db_user,
    password: db_password
};

//domain config
config.domainServer = "domain.yoursite.com";
config.domainPort = 9802;

config.logLevel = 'info';
config.logFile = '/sandbox/logs/log.txt';

config.jwtSecKey = 'jinyexin';
config.jwtTimeout = '90d';

config.imagePath = "/sandbox/temp";

module.exports = config;
