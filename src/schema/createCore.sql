create table user(
	id int NOT NULL AUTO_INCREMENT,
	username varchar(64) not null unique ,
	password varchar(64) not null,
    email varchar(64) default '',
    create_date DATETIME,
    update_date TIMESTAMP,
    creator bigint(20) DEFAULT -1 comment '-1:system',
    updater bigint(20) DEFAULT -1 comment '-1:system',
    PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;