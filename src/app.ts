/**
 * Created by enixjin on 2/10/2019.
 */
import {logger} from "./util";
import {IController} from "./controller/decorator";

export class App {

    constructor(public _appConfig: appConfig) {
    }

    init() {
        logger.info("=====================================");
        logger.info(`==       start ${this._appConfig.appName} server.      ==`);
        logger.info("=====================================");
        let server = require("./server");
        server.start(
            this._appConfig.controllers.map(c => new c().getRouter()),
            this._appConfig.gateway,
            this._appConfig.initialDatabase,
            this._appConfig.enableSwagger,
            this._appConfig.endpoint
        );
    }
}

export class appConfig {
    appName?: string = "domain";
    controllers: Array<new () => IController>;
    gateway?: new () => any = null;
    initialDatabase? = true;
    enableSwagger? = true;
    endpoint? = "/api";
}
