/**
 * Created by enixjin on 2/10/2019.
 */

import * as swaggerUI from "swagger-ui-express";
import * as express from "express";
import {defaultException, initDB, logger, serviceException} from "./";

function start(routers: express.Router[], gateway: new () => any, initialDatabase = true, enableSwagger = true, endpoint = "/api") {

    require('source-map-support').install();
    let config = global.config;

    if (initialDatabase) {
        initDB(config.db);
    }


    let path = require('path');
    let favicon = require('serve-favicon');
    let morgan = require('morgan');
    //let cookieParser = require('cookie-parser');
    let bodyParser = require('body-parser');

    let app = express();
    let http = require('http');

    // view engine setup
    // app.set('views', path.join(__dirname, 'views'));
    // app.set('view engine', 'ejs');

    // uncomment after placing your favicon in /public
    //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    //app.use(logger('dev'));
    app.use(morgan('combined', {
        skip: function (req, res) {
            return res.statusCode < 400
        }
    }));
    app.use(bodyParser.json());
    app.use(bodyParser.text());
    app.use(bodyParser.urlencoded({extended: false}));
    //app.use(cookieParser());
    //app.use(express.static('./public'));

    if (config.imagePath) {
        app.use('/image', express.static(config.imagePath));
    }

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,jwt");
        res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
        if (req.method === "OPTIONS") {
            res.end();
        } else {
            next();
        }
    });

    if (enableSwagger) {
        const YAML = require('yamljs');
        let swaggerDocument = YAML.load('./swagger.yaml');
        app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));
    }

    app.use(endpoint, routers);

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        let err = defaultException.ERR_404;
        next(err);
    });

    // error handlers
    app.use(function (err, req, res, next) {
        if (err instanceof serviceException) {
            res.status(err.statusCode);
            res.jsonp({message: err.message});
        } else {
            res.status(err.statusCode ? err.statusCode : 500);
            logger.error(JSON.stringify(err));
            res.jsonp({message: err});
        }
    });

    /**
     * Get port from environment and store in Express.
     */

    let port = normalizePort(config.servicePort || '3000');
    app.set('port', port);

    /**
     * Create HTTP server.
     */

    let server = http.createServer(app);

    if (gateway) {
        let socketGateway = new gateway();
        socketGateway.init(server);
    }

    /**
     * Listen on provided port, on all network interfaces.
     */

    server.listen(port);
    server.on('error', onError);
    server.on('listening', onListening);

    process.on('unhandledRejection', (reason, p) => {
        logger.error(`Unhandled Rejection at: Promise ${p} reason: ${reason}`);
    });

    /**
     * Normalize a port into a number, string, or false.
     */

    function normalizePort(val) {
        let port = parseInt(val, 10);

        if (isNaN(port)) {
            // named pipe
            return val;
        }

        if (port >= 0) {
            // port number
            return port;
        }

        return false;
    }

    /**
     * Event listener for HTTP server "error" event.
     */

    function onError(error) {
        if (error.syscall !== 'listen') {
            throw error;
        }

        let bind = typeof port === 'string'
            ? 'Pipe ' + port
            : 'Port ' + port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                console.error(bind + ' requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(bind + ' is already in use');
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    /**
     * Event listener for HTTP server "listening" event.
     */

    function onListening() {
        logger.info("Server started at " + config.servicePort);
    }

}

module.exports = {
    start: start
};
