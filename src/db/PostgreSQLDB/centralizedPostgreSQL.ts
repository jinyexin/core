/**
 * Created by Enix on 3/11/2016.
 */
import * as pg from 'pg';
import {PostgreSQLDB} from "./PostgreSQLDB";
import {logger} from "../../util/logger";

export class centralizedPostgreSQL extends PostgreSQLDB {
    pool: pg.Pool;
    private static _instance: centralizedPostgreSQL = new centralizedPostgreSQL();

    constructor() {
        super();
        // winston.info('init centralizedPostgreSQL');
        if (centralizedPostgreSQL._instance) {
            throw new Error("Error: Instantiation failed: Use getInstance() instead of new.");
        }
        centralizedPostgreSQL._instance = this;
    }

    public static getInstance(): centralizedPostgreSQL {
        return centralizedPostgreSQL._instance;
    }

    public init() {
        let config: any = global.config;
        this.pool = this.createPool(config.centralizedDB, 'centralized');
    }


    getPool(param: any): pg.Pool {
        logger.silly("in database:[centralized]");
        return this.pool;
    }
}
