/**
 * Created by enixjin on 12/20/16.
 */
import * as pg from "pg";
import {serviceException} from "../../exception/serviceException";
import {baseDB} from "../baseDB";
import {logger} from "../../util/logger";

export abstract class PostgreSQLDB implements baseDB {
    createPool(cfg, name): pg.Pool {
        let pool = new pg.Pool({
            max: cfg.connectionLimit,
            host: cfg.host,
            database: cfg.database,
            user: cfg.user,
            password: cfg.password
        });
        logger.debug("PostgreSQL database(" + name + ") at:" + cfg.host + " pool created.");
        return pool;
    }

    abstract getPool(param: any): pg.Pool;

    abstract init();

    public query(sql: string, param: any, indicator?: string): Promise<any> {
        logger.silly('execute sql:' + sql);
        logger.silly('with param:' + JSON.stringify(param));
        return new Promise<any>((resolve, reject) => {
            this.getPool(indicator).query(sql, param, (err, rows) => {
                if (err) {
                    logger.error("while execute SQL: " + sql);
                    logger.error("with params: " + JSON.stringify(param));
                    logger.error(err.toString());
                    reject(new serviceException("Database failure!", 500, err.toString()));
                } else {
                    resolve(rows);
                }
            });
        });
    }
}