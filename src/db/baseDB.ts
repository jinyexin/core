import * as fs from "fs-extra";
/**
 * Created by Enix on 3/11/2016.
 */
import * as mysql from "mysql";
import * as pg from "pg";
import {centralizedMySQL} from "./MySQL/centralizedMySQL";
import {distributedMySQL} from "./MySQL/distributedMySQL";
import {centralizedPostgreSQL} from "./PostgreSQLDB/centralizedPostgreSQL";

export interface baseDB {
    /**
     * a method to creat pool
     * @param cfg database config
     * @param name db name
     */
    createPool(cfg, name): mysql.IPool | pg.Pool;

    getPool(param: any): mysql.IPool | pg.Pool;

    init();

    query(sql: string, param: any, indicator?: string): Promise<any>;
}

export function initDB(type: "MySQL" | "PostgreSQL" = "MySQL") {
    const config = global.config;
    switch (type) {
        case "MySQL":
            if (config.centralizedDB) {
                centralizedMySQL.getInstance().init();
            }
            if (config.distributedDBs) {
                distributedMySQL.getInstance().init();
            }
            break;
        case "PostgreSQL":
            if (config.centralizedDB) {
                centralizedPostgreSQL.getInstance().init();
            }
            break;
    }
}

export enum dbType {
    distributed, centralized
}