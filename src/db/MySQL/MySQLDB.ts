/**
 * Created by enixjin on 12/20/16.
 */
import * as mysql from "mysql";
import {serviceException} from "../../exception/serviceException";
import {baseDB} from "../baseDB";
import {logger} from "../../util/logger";

export abstract class MySQLDB implements baseDB {
    createPool(cfg, name): mysql.IPool {
        let pool = mysql.createPool({
            connectionLimit: cfg.connectionLimit,
            host: cfg.host,
            port: cfg.port ? cfg.port : 3306,
            database: cfg.database,
            user: cfg.user,
            password: cfg.password,
            dateStrings: true,
            charset: cfg.charset ? cfg.charset : "utf8mb4_unicode_ci"
        });
        logger.debug("MySQL database pool(" + name + ") at:" + cfg.host + " created.");
        return pool;
    }

    abstract getPool(param: any): mysql.IPool;

    abstract init();

    public query(sql: string, param: any, indicator?: string): Promise<any> {
        logger.silly('execute sql:' + sql);
        logger.silly('with param:' + JSON.stringify(param));
        return new Promise<any>((resolve, reject) => {
            this.getPool(indicator).query(sql, param, (err, rows) => {
                if (err) {
                    logger.error("while execute SQL: " + sql);
                    logger.error("with params: " + JSON.stringify(param));
                    logger.error(err.toString());
                    reject(new serviceException("Database failure!", 500, err.toString()));
                } else {
                    resolve(rows);
                }
            });
        });
    }
}
