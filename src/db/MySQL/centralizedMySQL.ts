/**
 * Created by Enix on 3/11/2016.
 */
import * as mysql from 'mysql';
import {MySQLDB} from "./MySQLDB";
import {logger} from "../../util/logger";

export class centralizedMySQL extends MySQLDB {
    pool: mysql.IPool;
    private static _instance: centralizedMySQL = new centralizedMySQL();

    constructor() {
        super();
        // winston.info('init centralizedMySQL');
        if (centralizedMySQL._instance) {
            throw new Error("Error: Instantiation failed: Use getInstance() instead of new.");
        }
        centralizedMySQL._instance = this;
    }

    public static getInstance(): centralizedMySQL {
        return centralizedMySQL._instance;
    }

    public init() {
        let config: any = global.config;
        if (config.centralizedDB) {
            this.pool = this.createPool(config.centralizedDB, 'centralized');
        } else {
            logger.warn("centralizedDB config missing...start with no db mode!");
        }
    }


    getPool(param: any): mysql.IPool {
        logger.silly("in database:[centralized]");
        return this.pool;
    }
}
