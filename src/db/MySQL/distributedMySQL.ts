/**
 * Created by Enix on 3/11/2016.
 */
import * as mysql from "mysql";
import {tools} from "../../util/tools";
import {MySQLDB} from "./MySQLDB";
import {logger} from "../../util/logger";

export class distributedMySQL extends MySQLDB {
    pools: mysql.IPool[] = [];
    private static _instance: distributedMySQL = new distributedMySQL();

    constructor() {
        super();
        // winston.info('init distributedMySQL');
        if (distributedMySQL._instance) {
            throw new Error("Error: Instantiation failed: Use getInstance() instead of new.");
        }
        distributedMySQL._instance = this;
    }

    public static getInstance(): distributedMySQL {
        return distributedMySQL._instance;
    }

    public init() {
        let config: any = global.config;
        if (config.distributedDBs) {
            config.distributedDBs.map(
                cfg => {
                    this.pools.push(this.createPool(cfg, 'distributed'));
                }
            );
        } else {
            logger.warn("distributedDB config missing...start with no db mode!");
        }
    }


    getPool(indicator: any): mysql.IPool {
        //if there is only one data db, just return that db
        if (this.pools.length === 1) {
            return this.pools[0];
        }
        if (!indicator) {
            logger.error("Implementation Error: A query to distributed db should always give indicator");
            throw new Error("A query to distributed db should always give indicator");
        }
        return this.getDBByIndicator(indicator);
    }

    getDBByIndicator(indicator: any) {
        let dbHash = tools.getHash(indicator);
        logger.silly(`in database:[distributed][${dbHash}]`);
        return this.pools[dbHash];
    }

}
