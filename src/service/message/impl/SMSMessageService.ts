/**
 * Created by enixjin on 16-12-29.
 */
import {messageService} from "../messageService";
import {logger} from "../../../util/logger";

export class SMSMessageService implements messageService {
    private static _instance: SMSMessageService = new SMSMessageService();

    public getInstance(): SMSMessageService {
        return SMSMessageService._instance;
    }

    pushMessage(message: any): boolean {
        //TBD
        logger.info("send message to phone.");
        return null;
    }
}
