/**
 * Created by enixjin on 16-12-29.
 */
import {messageService} from "../messageService";
import {logger} from "../../../util/logger";

export class EmailMessageService implements messageService {
    private static _instance: EmailMessageService = new EmailMessageService();

    public getInstance(): EmailMessageService {
        return EmailMessageService._instance;
    }

    pushMessage(message: any): boolean {
        // let transporter = nodemailer.createTransport({
        //     service: '163',
        //     auth: {
        //         user: 'yourname@163.com',
        //         pass: 'password'
        //     }
        // });
        //
        //
        // let mailOptions2 = {
        //     from: '"enixjin" <yourname@163.com>', // sender address
        //     to: 'enixjin@enixjin.net', // list of receivers
        //     subject: 'Hello ✔', // Subject line
        //     text: 'Hello world ?', // plain text body
        //     html: '<b>Hello world ?</b>' // html body
        // };
        //
        //
        // transporter.sendMail(mailOptions2, (error, info) => {
        //     if (error) {
        //         return console.log(error);
        //     }
        //     console.log('Message %s sent: %s', info.messageId, info.response);
        // });
        //TBD
        logger.info("send email.");
        return null;
    }
}