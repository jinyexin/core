/**
 * Created by enixjin on 16-12-29.
 */
import {messageService} from "../messageService";
import {logger} from "../../../util/logger";

export class WeChatMessageService implements messageService {
    private static _instance: WeChatMessageService = new WeChatMessageService();

    public getInstance(): WeChatMessageService {
        return WeChatMessageService._instance;
    }

    pushMessage(message: any): boolean {
        //TBD
        logger.info("push message to Wechat.");
        return null;
    }
}
