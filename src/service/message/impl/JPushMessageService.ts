/**
 * Created by enixjin on 16-12-29.
 */
import {messageService} from "../messageService";
import {logger} from "../../../util/logger";

export class JPushMessageService implements messageService {
    private static _instance: JPushMessageService = new JPushMessageService();

    public getInstance(): JPushMessageService {
        return JPushMessageService._instance;
    }

    pushMessage(message: any): boolean {
        //TBD
        logger.info("push message to App.");
        return null;
    }
}
