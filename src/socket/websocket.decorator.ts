/**
 * Created by enixjin on 2/11/18.
 */

import {Server} from "http";
import "reflect-metadata";
import {interval} from 'rxjs';
import * as SocketIO from "socket.io";
import {logger} from "../util";


export type sender = () => Promise<any>;
export type subscriber = (io: SocketIO.Server, socket: SocketIO.Socket, data: any) => Promise<any> | void;
export type sendOption = { interval: number, event: string, callback?: sender };
export type subscribeOption = { event: string, callback?: subscriber };
export const eventMappingMetadataKey = Symbol("eventMappingMetadataKey");
export const subscribeMappingMetadataKey = Symbol("subscribeMappingMetadataKey");

export function WebSocketGateway(): any {
    return function <T extends { new(...args: any[]): {} }>(constructor: T) {
        return class extends constructor {
            io: SocketIO.Server;

            init(server: Server) {
                this.io = SocketIO(server);
                global.dependencyInjectionContainer.set("socket.io", this.io);
                logger.info(`socket.io Server started`);

                //sender
                let eventMapping = <sendOption[]>Reflect.getMetadata(eventMappingMetadataKey, this);
                if (!eventMapping) {
                    eventMapping = [];
                }
                eventMapping.map(option => {
                    interval(option.interval).subscribe(
                        () => {
                            option.callback.call(this).then(
                                data => {
                                    this.io.emit(option.event, data);
                                }
                            );
                        }
                    );
                    logger.info(`binding socket sender: [interval:${option.interval},event:${option.event}]`);
                });

                //subscriber
                let subscribeMapping = <subscribeOption[]>Reflect.getMetadata(subscribeMappingMetadataKey, this);
                if (!subscribeMapping) {
                    subscribeMapping = [];
                }
                subscribeMapping.map(option => {
                    logger.info(`binding socket subscriber: [event:${option.event}]`);
                });
                this.io.on("connection", socket => {
                    subscribeMapping.map(option => {
                            socket.on(option.event, data => {
                                option.callback.call(this, this.io, socket, data);
                            });
                        }
                    );
                });

            }
        }
    }

}

export function SendMessage(option: sendOption) {
    return (target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<sender>) => {
        // save a reference to the original method this way we keep the values currently in the
        // descriptor and don't overwrite what another decorator might have done to the descriptor.
        if (descriptor === undefined) {
            descriptor = Object.getOwnPropertyDescriptor(target, propertyKey);
        }

        let originalMethod = descriptor.value;

        option.callback = originalMethod;

        let eventMapping = Reflect.getOwnMetadata(eventMappingMetadataKey, target);
        if (!eventMapping) {
            eventMapping = [];
        }
        eventMapping.push(option);
        Reflect.defineMetadata(eventMappingMetadataKey, eventMapping, target);
    }
}

export function SubscribeMessage(option: subscribeOption) {
    return (target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<subscriber>) => {
        // save a reference to the original method this way we keep the values currently in the
        // descriptor and don't overwrite what another decorator might have done to the descriptor.
        if (descriptor === undefined) {
            descriptor = Object.getOwnPropertyDescriptor(target, propertyKey);
        }

        option.callback = descriptor.value;

        let subscribeMapping = Reflect.getOwnMetadata(subscribeMappingMetadataKey, target);
        if (!subscribeMapping) {
            subscribeMapping = [];
        }
        subscribeMapping.push(option);
        Reflect.defineMetadata(subscribeMappingMetadataKey, subscribeMapping, target);
    }
}
