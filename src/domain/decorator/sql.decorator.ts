/**
 * Created by enixjin on 2/11/18.
 */

import "reflect-metadata";
import {baseDB, dbType} from "../../db";
import {logger} from "../../util";
import {PagedData, PageInfo} from "..";

export function SQL(sql: string, postProcess: (rows: any) => Promise<any> = v => Promise.resolve(v)) {
    return (target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<Function>) => {
        // save a reference to the original method this way we keep the values currently in the
        // descriptor and don't overwrite what another decorator might have done to the descriptor.
        if (descriptor === undefined) {
            descriptor = Object.getOwnPropertyDescriptor(target, propertyKey);
        }
        let originalMethod = descriptor.value;

        descriptor.value = function (...args: any[]) {
            if (!target.getDB) {
                logger.error(`using @SQL in a none repository class?`);
                throw new Error(`using @SQL in a none repository class?`);
            }
            //TODO check db type
            let db: baseDB = this.getDB(dbType.centralized);
            return db.query(sql, args).then(postProcess);
        }

    }

}

export function PagedSQL(sql: string, postProcess: (rows: any) => Promise<any> = v => Promise.resolve(v)) {
    return (target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<Function>) => {
        // save a reference to the original method this way we keep the values currently in the
        // descriptor and don't overwrite what another decorator might have done to the descriptor.
        if (descriptor === undefined) {
            descriptor = Object.getOwnPropertyDescriptor(target, propertyKey);
        }
        let originalMethod = descriptor.value;

        descriptor.value = function (pageInfo: PageInfo) {
            if (!target.getDB) {
                logger.error(`using @SQL in a none repository class?`);
                throw new Error(`using @SQL in a none repository class?`);
            }
            //TODO check db type
            let db: baseDB = this.getDB(dbType.centralized);
            if (!pageInfo.JSONQuery) {
                logger.error(`a customized PagedSQL must pass JSONQuery!`);
                throw new Error(`a customized PagedSQL must pass JSONQuery!`);
            }
            pageInfo.pageSize = pageInfo.pageSize ? pageInfo.pageSize : 10;
            pageInfo.offset = pageInfo.offset ? pageInfo.offset : 0;
            let result = new PagedData();
            return Promise.all([
                db.query(`select count(1) as count from (${sql}) s`, pageInfo.JSONQuery),
                db.query(`${sql} limit ${pageInfo.offset * pageInfo.pageSize},${pageInfo.pageSize}`, pageInfo.JSONQuery)
            ])
                .then(
                    (values) => {
                        pageInfo.total = values[0][0].count;
                        pageInfo.count = values[0][0].count;
                        result.page = pageInfo;
                        result.data = values[1];
                        return result;
                    },
                ).then(postProcess);
        }

    }

}

