import "reflect-metadata";
/**
 * Created by enixjin on 5/12/16.
 */
import {dbType} from "../../db/baseDB";

const exportMetadataKey = Symbol("exportMetadataKey");
const readOnlyMetadataKey = Symbol("readOnlyMetadataKey");
const writeOnlyMetadataKey = Symbol("writeOnlyMetadataKey");
const subQueryMetadataKey = Symbol("subQueryMetadataKey");
const subQuerysMetadataKey = Symbol("subQuerysMetadataKey");

export interface RESTEntityOption {
    table: string;
    URL: string;
    db?: dbType;
}

export function RESTEntity(option: RESTEntityOption) {
    if (!option.db) {
        option.db = dbType.centralized;
    }
    return function decorator(target) {
        target.prototype.getTableName = () => option.table;
        target.prototype.getRESTUrl = () => option.URL;
        target.prototype.getDBType = () => option.db;
    };
}

//exportable
export function exportable(value: boolean) {
    return Reflect.metadata(exportMetadataKey, value);
}

export function getExportable(target: any, propertyKey: string) {
    return Reflect.getMetadata(exportMetadataKey, target, propertyKey);
}

export function readOnly() {
    return Reflect.metadata(readOnlyMetadataKey, true);
}

export function getReadOnly(target: any, propertyKey: string) {
    return Reflect.getMetadata(readOnlyMetadataKey, target, propertyKey);
}

export function writeOnly() {
    return Reflect.metadata(writeOnlyMetadataKey, true);
}

export function getWriteOnly(target: any, propertyKey: string) {
    return Reflect.getMetadata(writeOnlyMetadataKey, target, propertyKey);
}

//subQuery
export function subQuery(value: { name: string, repository: any }) {
    return Reflect.metadata(subQueryMetadataKey, value);
}

export function getSubQuery(target: any, propertyKey: string) {
    return Reflect.getMetadata(subQueryMetadataKey, target, propertyKey);
}

//subQuerys
export function subQuerys(value: { name: string, child_id_column: string, repository: string }) {
    return Reflect.metadata(subQuerysMetadataKey, value);
}

export function getSubQuerys(target: any, propertyKey: string) {
    return Reflect.getMetadata(subQuerysMetadataKey, target, propertyKey);
}