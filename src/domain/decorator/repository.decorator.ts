/**
 * Created by enixjin on 2/11/18.
 */
import {centralizedMySQL} from "../../db/MySQL/centralizedMySQL";
import {baseDB} from "../../db/baseDB";
import {logger} from "../../util/logger";

export function Repository(name?: string) {
    return function (target) {
        let injectName = name ? name : target.name;
        global.dependencyInjectionContainer.set(injectName, new target());
    }
}

export function BaseRepository(name?: string) {
    return function (target) {
        logger.warn("BaseRepository", target.name);

        Object.defineProperty(target.prototype, 'centralizedDB', {
            value: centralizedMySQL.getInstance()
        });

        Object.defineProperty(target.prototype, 'getDB', {
            value: function () {
                return this.centralizedDB;
            }
        });

        let injectName = name ? name : target.name;
        global.dependencyInjectionContainer.set(injectName, new target());

        return target;
    };
}

export interface IRepository {
    getDB(): baseDB;
}