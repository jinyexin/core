/**
 * Created by enixjin on 12/13/16.
 */
import * as jwt from "jsonwebtoken";
import {baseRepository} from "./baseRepository";
import {encryption} from "../../util/encryption";
import {baseUser} from "../entity/baseUser";
import {logger} from "../../util/logger";

export abstract class baseUserRepository<T extends baseUser> extends baseRepository<T> {

    update(user: Partial<T>, id: number): Promise<number> {
        let db = this.getDB(this.domainObject.getDBType());
        if (user.password) {
            user.password = encryption.encryptMd5(user.password);
        }
        return db.query(
            "update " + this.domainObject.getTableName() + " set ? where id=?",
            [this.removeReadOnly(user), id]
        ).then(
            result => result.affectedRows
        );
    }

    login(username: string, password: string): Promise<any> {
        let encryptedPassword = encryption.encryptMd5(password);
        let db = this.getDB(this.domainObject.getDBType());
        return db.query(
            "select * from " + this.domainObject.getTableName() + " where username=? and password=?",
            [username, encryptedPassword]
        ).then(
            (result) => {
                if (result.length !== 1) {
                    return Promise.resolve(null);
                } else {
                    return this.postProcess(result[0]).then(
                        user => {
                            return {id: user.id, token: this.generateJWT(user), detail: user};
                        }
                    );
                }
            }
        );
    }

    changeNewPassword(newpassword: string, userid: any) {
        let db = this.getDB(this.domainObject.getDBType());
        let psw = encryption.encryptMd5(newpassword);
        return db.query(
            "update " + this.domainObject.getTableName() + " set password=? where id=?",
            [psw, userid]
        ).then(
            result => result.affectedRows
        );
    }

    generateJWT(user: Partial<T>): string {
        logger.silly("generate token for user[id:" + user.id + ",username:" + user.username + ",role:" + user.role + "]");
        let config = global.config;
        return jwt.sign({
            id: user.id,
            username: user.username,
            role: user.role
        } as any, config.jwtSecKey, {expiresIn: config.jwtTimeout});
    }
}