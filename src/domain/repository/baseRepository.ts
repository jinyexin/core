/**
 * Created by Enix on 3/15/2016.
 */
import {
    baseDB,
    baseDomainObject,
    centralizedMySQL,
    dbType,
    distributedMySQL,
    getExportable, getReadOnly,
    getSubQuery,
    getSubQuerys, getWriteOnly,
    PageInfo,
    serviceException
} from "../..";
import {logger} from "../../util";

export abstract class baseRepository<T extends baseDomainObject> {
    domainObject: T;
    centralizedDB: centralizedMySQL = centralizedMySQL.getInstance();
    distributedDB: distributedMySQL = distributedMySQL.getInstance();

    protected constructor(protected _domainObject) {
        this.domainObject = new this._domainObject();
    }

    getDomainObject(): T {
        return this.domainObject;
    }

    /**
     * insert a new record to database
     * @param objectToInsert
     * @param indicator? database indicator if any
     * @returns {Promise<number>}
     */
    async insert(objectToInsert: Partial<T>, indicator?: string): Promise<number> {
        let db = this.getDB(this.domainObject.getDBType());
        const result = await db.query("insert into " + this.domainObject.getTableName() + " set ?", objectToInsert, indicator);
        return result.insertId;
    }

    /**
     * insert a set of entity to database
     * @param objectsToInsert
     * @param indicator
     * @returns {Promise<number>}
     */
    async batchInsert(objectsToInsert: Array<Partial<T>>, indicator?: string): Promise<number> {
        let db = this.getDB(this.domainObject.getDBType());
        let keys = Object.keys(objectsToInsert[0]);
        let data = [];
        objectsToInsert.forEach(
            (domainObject) => {
                let values = [];
                keys.map((key => {
                    values.push(domainObject[key]);
                }));
                data.push(values);
            }
        );
        const rows = await db.query("insert into " + this.domainObject.getTableName() + " (" + keys.join() + ") values ?", [data], indicator);
        return rows.affectedRows;
    }

    async queryByID(id: number, postProcess: boolean = true, indicator?: string): Promise<T> {
        let db = this.getDB(this.domainObject.getDBType());
        const rows = await db.query("select * from " + this.domainObject.getTableName() + " where id=?", [id], indicator);
        if (rows.length !== 1) {
            return Promise.reject(new serviceException("Invalid id", 404, "The specified ID is invalid"));
        } else {
            if (postProcess) {
                return this.postProcess<T>((<T>rows[0]));
            } else {
                return Promise.resolve((<T>rows[0]));
            }
        }
    }

    /**
     * list all records
     * @param postProcess should data be post processed, default is true
     * @param limit limit result row number, default is true
     * @param indicator
     * @returns {Promise<any[]>}
     */
    async listAll(postProcess: boolean = true, limit: boolean = false, indicator?: string): Promise<T[]> {
        let db = this.getDB(this.domainObject.getDBType());
        let _limit = " ";
        if (limit) {
            _limit = " limit 100 ";
        }
        const rows = await db.query("select * from " + this.domainObject.getTableName() + " order by id desc " + _limit, [], indicator);
        if (postProcess) {
            return Promise.all<T>(rows.map(this.postProcess.bind(this)));
        } else {
            return Promise.resolve(rows);
        }
    }

    async update(objectToUpdate: Partial<T>, id: number, indicator?: string): Promise<number> {
        let db = this.getDB(this.domainObject.getDBType());
        const result = await db.query("update " + this.domainObject.getTableName() + " set ? where id=?", [this.removeReadOnly(objectToUpdate), id], indicator);
        return result.affectedRows;
    }

    batchUpdate(objects: Array<Partial<T>>, indicator?: string): Promise<any> {
        return Promise.all(objects.map(o => this.update(o, o.id, indicator)));
    }


    /**
     * Allow update records by condition
     * e.g. update table set {name:"enix"} where {name:"enixjin"} and {other:"\{1,2\}"}
     * @param dataToUpdate columns to update
     * @param query all query will be connect by and, {1,2} will be translate to in (1,2)
     * @param indicator db indecator
     * @returns {Promise<number>} update record numbers
     */
    async updateByCondition(dataToUpdate: Partial<T>, query: any, indicator?: string): Promise<number> {
        let where = " ";
        let first = true;
        let arg: any[] = [this.removeReadOnly(dataToUpdate)];
        Object.keys(query).forEach((key) => {
            if (first) {
                where += " where " + key + this.getConditionKey(query[key]);
                first = false;
            } else {
                where += " and " + key + this.getConditionKey(query[key]);
            }
            Array.prototype.push.apply(arg, this.getConditionArg(query[key]));
        });
        let db = this.getDB(this.domainObject.getDBType());
        const result = await db.query("update " + this.domainObject.getTableName() + " set ? " + where, arg, indicator);
        return result.affectedRows;
    }


    async delete(id: number, indicator?: string): Promise<number> {
        let db = this.getDB(this.domainObject.getDBType());
        const result = await db.query("delete from " + this.domainObject.getTableName() + " where id=?", [id], indicator);
        return result.affectedRows;
    }

    async batchDelete(query: Partial<T> | any, indicator?: string): Promise<number> {
        let where = " ";
        let first = true;
        let arg: any[] = [];
        Object.keys(query).forEach((key) => {
            if (first) {
                where += " where " + key + this.getConditionKey(query[key]);
                first = false;
            } else {
                where += " and " + key + this.getConditionKey(query[key]);
            }
            Array.prototype.push.apply(arg, this.getConditionArg(query[key]));
        });
        let db = this.getDB(this.domainObject.getDBType());
        const result = await db.query("delete from " + this.domainObject.getTableName() + where, arg, indicator);
        return result.affectedRows;
    }

    /**
     * search by query all are combined by 'and'
     * @param query
     * @param limit default is false
     * @param indicator
     * @returns {Promise<any[]>}
     */
    async search(query: Partial<T> | any, limit: boolean = false, indicator?: string): Promise<T[]> {
        let where = " ";
        let first = true;
        let arg: any[] = [];
        let _limit = "";
        if (limit) {
            _limit = " limit 100 ";
        }
        Object.keys(query).forEach((key) => {
            if (first) {
                where += " where " + key + this.getConditionKey(query[key]);
                first = false;
            } else {
                where += " and " + key + this.getConditionKey(query[key]);
            }
            Array.prototype.push.apply(arg, this.getConditionArg(query[key]));
        });
        let db = this.getDB(this.domainObject.getDBType());
        const rows = await db.query("select * from " + this.domainObject.getTableName() + where + " order by id desc " + _limit, arg, indicator);
        return Promise.all<T>(rows.map(this.postProcess.bind(this)));
    }

    /**
     * search like by query, all are combined by 'and'
     * @param query
     * @param limit default is false
     * @param indicator
     * @returns {Promise<any[]>}
     */
    async searchLike(query: Partial<T> | any, limit: boolean = false, indicator?: string): Promise<T[]> {
        let where = " ";
        let first = true;
        let arg: any[] = [];
        let _limit = "";
        if (limit) {
            _limit = " limit 100 ";
        }
        Object.keys(query).forEach((key) => {
            if (first) {
                where += " where " + key + " like ? ";
                first = false;
            } else {
                where += " and " + key + " like ? ";
            }
            arg.push('%' + query[key] + '%');
        });
        let db = this.getDB(this.domainObject.getDBType());
        const rows = await db.query("select * from " + this.domainObject.getTableName() + where + " order by id desc " + _limit, arg, indicator);
        return Promise.all<T>(rows.map(this.postProcess.bind(this)));
    }

    async searchLikeOr(query: Partial<T> | any, limit: boolean = false, indicator?: string): Promise<T[]> {
        let where = " ";
        let first = true;
        let arg: any[] = [];
        let _limit = "";
        if (limit) {
            _limit = " limit 100 ";
        }
        Object.keys(query).forEach((key) => {
            if (first) {
                where += " where " + key + " like ? ";
                first = false;
            } else {
                where += " or " + key + " like ? ";
            }
            arg.push('%' + query[key] + '%');
        });
        let db = this.getDB(this.domainObject.getDBType());
        const rows = await db.query("select * from " + this.domainObject.getTableName() + where + " order by id desc " + _limit, arg, indicator);
        return Promise.all<T>(rows.map(this.postProcess.bind(this)));
    }

    async count(indicator?: string): Promise<number> {
        let db = this.getDB(this.domainObject.getDBType());
        const result = await db.query("select count(1) as count from " + this.domainObject.getTableName(), [], indicator);
        return result[0].count;
    }

    async countAllLike(pageInfo: PageInfo, indicator?: string): Promise<number> {
        let db = this.getDB(this.domainObject.getDBType());

        let arg: any[] = [];
        let where = this.extractWhereFromPageInfo(pageInfo, arg);

        const result = await db.query("select count(1) as count from " + this.domainObject.getTableName() + where, arg, indicator);
        return result[0].count;
    }

    async queryAllLike(pageInfo: PageInfo, indicator?: string): Promise<T[]> {
        let db = this.getDB(this.domainObject.getDBType());

        let arg: any[] = [];
        let order_by = " order by id desc ";

        //search all columns
        let where = this.extractWhereFromPageInfo(pageInfo, arg);

        // sort, current only support first one
        if (pageInfo.sorts && pageInfo.sorts.length > 0) {
            order_by = ` order by ${pageInfo.sorts[0].column} ${pageInfo.sorts[0].dir}`;
        }

        arg.push(pageInfo.offset * pageInfo.pageSize);
        arg.push(pageInfo.pageSize);
        const rows = await db.query("select * from " + this.domainObject.getTableName() + where + order_by + " limit ?,?", arg, indicator);
        return Promise.all<T>(rows.map(this.postProcess.bind(this)));
    }

    private extractWhereFromPageInfo(pageInfo: PageInfo, arg: any[]): string {
        let where = "";
        if (pageInfo.columns && pageInfo.columns.length > 0) {
            where = " where (";
            let first = true;
            for (let column of pageInfo.columns) {
                if (first) {
                    first = false;
                    where += column.name + " like ?";
                } else {
                    where += ` ${column.join ? column.join : "or"}  ${column.name} like ?`;
                }
                arg.push(`${column.matchExactly ? "" : "%"}${column.value ? column.value : pageInfo.searchAllColumn}${column.matchExactly ? "" : "%"}`);
            }
            where += ") ";
        }
        return where;
    }

    //utils functions
    protected getDB(type: dbType): baseDB {
        if (type === dbType.distributed) {
            return this.distributedDB;
        } else if (type === dbType.centralized) {
            return this.centralizedDB;
        }
    }

    //post process to do something like not exportable column
    async postProcess<T>(domain: T): Promise<T> {
        await Promise.all([
            this.removeWriteOnly(domain),
            this.doSubQuery(domain),
            this.doSubQuerys(domain)
        ]);
        return domain;
    }

    async doSubQuery(domain): Promise<any> {
        let subQuerys = [];
        for (let key in domain) {
            if (getSubQuery(this.domainObject, key)) {
                let handler = getSubQuery(this.domainObject, key);
                if (domain[key]) {
                    if (!global.dependencyInjectionContainer.get(handler.repository)) {
                        logger.error(`cannot find [${handler.repository}] for injecting, maybe you forget to add @Service to that class?`);
                        throw new Error(`cannot find [${handler.repository}] for injecting, maybe you forget to add @Service to that class?`);
                    }
                    subQuerys.push(global.dependencyInjectionContainer.get(handler.repository).queryByID(domain[key]).then(
                        subObject => {
                            domain[handler.name] = subObject;
                            return domain;
                        }
                    ));
                }
            }
        }
        await Promise.all(subQuerys);
        return domain;
    }

    async doSubQuerys(domain): Promise<any> {
        let subQuerys = [];
        for (let key in domain) {
            if (getSubQuerys(this.domainObject, key)) {
                let handler = getSubQuerys(this.domainObject, key);
                let search = {};
                search[handler.child_id_column] = domain.id;
                if (!global.dependencyInjectionContainer.get(handler.repository)) {
                    logger.error(`cannot find [${handler.repository}] for injecting, maybe you forget to add @Service to that class?`);
                    throw new Error(`cannot find [${handler.repository}] for injecting, maybe you forget to add @Service to that class?`);
                }
                subQuerys.push(global.dependencyInjectionContainer.get(handler.repository).search(search).then(
                    subObject => {
                        domain[handler.name] = subObject;
                        return domain;
                    }
                ));
            }
        }
        await Promise.all(subQuerys);
        return domain;
    }

    //remove all write only fields like password
    removeWriteOnly(objectToShow: Partial<T>): Promise<any> {
        for (let key in objectToShow) {
            if (getExportable(this.domainObject, key) === false || getWriteOnly(this.domainObject, key) === true) {
                delete objectToShow[key];
            }
        }
        return Promise.resolve(objectToShow);
    }

    //remove all read only fields like id or update_date
    removeReadOnly(objectToUpdate: Partial<T>) {
        for (let key in objectToUpdate) {
            if (getReadOnly(this.domainObject, key) === true) {
                delete objectToUpdate[key];
            }
        }
        return objectToUpdate;
    }

    getFields(objectToInsert): string[] {
        let result = [];
        Object.getOwnPropertyNames(objectToInsert)
            .forEach((field) => {
                result.push(field);
            });
        return result;
    }

    getConditionKey(value: string): string {
        if (value.toString().indexOf('{') >= 0) {
            let result = " in (";
            let first = true;
            value.substring(value.indexOf('{') + 1, value.length - 1).split(",").forEach(
                () => {
                    if (first) {
                        result += "?";
                        first = false;
                    } else {
                        result += ",?";
                    }
                }
            );
            return result + ") "
        } else {
            return "=? ";
        }
    }

    getConditionArg(value: string): string[] {
        if (value.toString().indexOf('{') == 0) {
            let q = value.substring(value.indexOf('{') + 1, value.length - 1);
            return q.split(",");
        } else {
            return [value];
        }
    }
}
