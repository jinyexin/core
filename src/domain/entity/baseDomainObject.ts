/**
 * Created by Enix on 3/14/2016.
 */
import {dbType} from "../../db/baseDB";
import {readOnly} from "../decorator/entity.decorator";

export abstract class baseDomainObject {
    @readOnly()
    id: number;

    creator: number;
    updater: number;
    create_date: Date;
    @readOnly()
    update_date: Date;

    getTableName(): string {
        throw new Error('table name of this object is not defined!');
    }

    getRESTUrl(): string {
        throw new Error('REST URL of this object is not defined!');
    }

    getDBType(): dbType {
        throw new Error('dbType of this object is not defined!');
    }

}
