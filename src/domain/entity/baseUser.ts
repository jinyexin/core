/**
 * Created by enixjin on 12/13/16.
 */
import {exportable, writeOnly} from "../decorator/entity.decorator";
import {baseDomainObject} from "./baseDomainObject";

export abstract class baseUser extends baseDomainObject {
    //@subQuerys({name: "eventUser", child_id_column: "event_id", repository: eventUserRepository})
    username: string;

    @writeOnly()
    password: string;

    role: number;

    //WARNNING: sub query should be carefully used to avoid entity infinite load each other
    //@subQuery will call xxxRepository's queryByID method to fetch entity, if not found return null.

    // @subQuery({name: "group", repository: groupRepository})
    // group_id: number;
}