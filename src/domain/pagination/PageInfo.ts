/**
 * Created by enixjin on 3/28/18.
 */

export class PageInfo {
    pageSize = 0;
    count = 0; //search result total
    total = 0;
    offset = 0; //pageNumber
    searchAllColumn: string = "";
    sorts?: sort[];
    columns?: [columnSearch];
    JSONQuery?: any;
}

export class columnSearch {
    name: string;
    value?: string;
    matchExactly?: boolean;
    join?: "or" | "and";
}

export class sort {
    dir: "asc" | "desc";
    column: string;
}