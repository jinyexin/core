/**
 * Created by enixjin on 3/28/18.
 */
import {PageInfo} from "./PageInfo";

export class PagedData<T> {

    data: T[] = [];
    page = new PageInfo();

}
