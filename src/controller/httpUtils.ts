/**
 * Created by enixjin on 2/12/18.
 */
import * as jwt from "jsonwebtoken";
import {RESTAPI} from "./decorator";
import {baseRepository, PagedData, PageInfo} from "../domain";
import {apiService, encryption, logger} from "../util";
import {defaultException, serviceException} from "../exception";


export namespace HttpUtils {

    export function Post(apiDefinition: RESTAPI, __this) {
        apiService.getInstance().push(apiDefinition);
        __this.router.post(apiDefinition.url, apiDefinition.callbacks.map(c => c.bind(__this)));
    }

    export function Get(apiDefinition: RESTAPI, __this) {
        apiService.getInstance().push(apiDefinition);
        __this.router.get(apiDefinition.url, apiDefinition.callbacks.map(c => c.bind(__this)));
    }

    export function Put(apiDefinition: RESTAPI, __this) {
        apiService.getInstance().push(apiDefinition);
        __this.router.put(apiDefinition.url, apiDefinition.callbacks.map(c => c.bind(__this)));
    }

    export function Delete(apiDefinition: RESTAPI, __this) {
        apiService.getInstance().push(apiDefinition);
        __this.router.delete(apiDefinition.url, apiDefinition.callbacks.map(c => c.bind(__this)));
    }

    export function Patch(apiDefinition: RESTAPI, __this) {
        apiService.getInstance().push(apiDefinition);
        __this.router.patch(apiDefinition.url, apiDefinition.callbacks.map(c => c.bind(__this)));
    }

    export function Options(apiDefinition: RESTAPI, __this) {
        apiService.getInstance().push(apiDefinition);
        __this.router.options(apiDefinition.url, apiDefinition.callbacks.map(c => c.bind(__this)));
    }

    export function All(apiDefinition: RESTAPI, __this) {
        apiService.getInstance().push(apiDefinition);
        __this.router.all(apiDefinition.url, apiDefinition.callbacks.map(c => c.bind(__this)));
    }

    export function createCRUD(RESTUrl: string, __this, auth) {
        // winston.info("generating CRUD for:" + RESTUrl);
        //get List

        let authentication = auth ? checkAuthentication : (req, res, next) => {
            next();
        };

        HttpUtils.Get({
            method: "GET",
            url: RESTUrl,
            callbacks: [authentication, (req, res, next) => {
                let limit = !(req.query.__limit === "false");
                delete req.query.__limit;
                if (req.query) {
                    if (req.query.__mode) {
                        if (req.query.__mode === "like") {
                            delete req.query.__mode;
                            __this.repository.searchLike(req.query, limit).then(
                                (rows) => res.status(200).jsonp(rows),
                                (error) => HttpUtils.handleError(error, res)
                            );
                        } else { //==="likeOr"
                            delete req.query.__mode;
                            __this.repository.searchLikeOr(req.query, limit).then(
                                (rows) => res.status(200).jsonp(rows),
                                (error) => HttpUtils.handleError(error, res)
                            );
                        }
                    } else {
                        __this.repository.search(req.query, limit).then(
                            (rows) => res.status(200).jsonp(rows),
                            (error) => HttpUtils.handleError(error, res)
                        );
                    }
                } else {
                    __this.repository.listAll(true, limit).then(
                        (rows) => res.status(200).jsonp(rows),
                        (error) => HttpUtils.handleError(error, res)
                    );
                }
            }]
            , auth
        }, __this);

        //get by id
        HttpUtils.Get({
            method: "GET",
            url: RESTUrl + "/:id",
            callbacks: [authentication, (req, res, next) => {
                if (Number(req.params.id)) {
                    __this.repository.queryByID(req.params.id).then(
                        (result) => {
                            res.status(200).jsonp(result);
                        },
                        (error) => HttpUtils.handleError(error, res)
                    );
                } else {
                    res.status(400).jsonp({
                        "message": "Invalid user id",
                        "statusCode": 400,
                        "error": "The specified user ID is not a number"
                    });
                }
            }]
            , auth
        }, __this);

        //create
        HttpUtils.Post({
            method: "POST",
            url: RESTUrl,
            callbacks: [authentication, (req, res, next) => {
                const usr = HttpUtils.getAuthUserSync(req);
                if (Array.isArray(req.body)) {
                    req.body.map((obj) => {
                        if (usr) {
                            obj.creator = usr.id;
                        }
                        obj.create_date = new Date();
                    });
                    __this.repository.batchInsert(req.body).then(
                        (insertID) => res.status(201).jsonp({affectedRows: insertID}),
                        (error) => HttpUtils.handleError(error, res)
                    );
                } else {
                    req.body.create_date = new Date();
                    if (usr) {
                        req.body.creator = usr.id;
                    }
                    __this.repository.insert(req.body)
                        .then(
                            (insertID) => res.status(201).jsonp({id: insertID}),
                            (error) => HttpUtils.handleError(error, res)
                        );
                }
            }]
            , auth
        }, __this);

        //update
        HttpUtils.Put({
            method: "PUT",
            url: RESTUrl + "/:id",
            callbacks: [authentication, (req, res, next) => {
                const usr = HttpUtils.getAuthUserSync(req);
                if (usr) {
                    req.body.updater = usr.id;
                }
                __this.repository.update(req.body, req.params.id).then(
                    (changedRows) => res.status(200).jsonp({affectedRows: changedRows}),
                    (error) => HttpUtils.handleError(error, res)
                );
            }]
            , auth
        }, __this);

        HttpUtils.Put({
            method: "PUT",
            url: RESTUrl,
            callbacks: [authentication, (req, res, next) => {
                const usr = HttpUtils.getAuthUserSync(req);
                req.body.forEach(o => {
                    if (usr) {
                        o.updater = usr.id;
                    }
                });
                __this.repository.batchUpdate(req.body).then(
                    (changedRows) => res.status(200).jsonp({affectedRows: changedRows.length}),
                    (error) => HttpUtils.handleError(error, res)
                );
            }]
            , auth
        }, __this);

        //delete
        HttpUtils.Delete({
            method: "DELETE",
            url: RESTUrl,
            callbacks: [authentication, (req, res, next) => {
                __this.repository.batchDelete(req.query).then(
                    (changedRows) => res.status(200).jsonp({affectedRows: changedRows}),
                    (error) => HttpUtils.handleError(error, res)
                );
            }]
            , auth
        }, __this);
        HttpUtils.Delete({
            method: "DELETE",
            url: RESTUrl + "/:id",
            callbacks: [authentication, (req, res, next) => {
                __this.repository.delete(req.params.id).then(
                    (changedRows) => res.status(200).jsonp({affectedRows: changedRows}),
                    (error) => HttpUtils.handleError(error, res)
                );
            }]
            , auth
        }, __this);

        HttpUtils.Post({
            method: "POST",
            url: RESTUrl + "/pagination",
            callbacks: [authentication, async (req, res, next) => {
                let pageInfo = req.body as PageInfo;
                let result = new PagedData();
                Promise.all([
                    __this.repository.count(),
                    __this.repository.countAllLike(pageInfo),
                    __this.repository.queryAllLike(pageInfo)
                ])
                    .then(
                        (values) => {
                            pageInfo.total = values[0];
                            pageInfo.count = values[1];
                            result.page = pageInfo;
                            result.data = values[2];
                            res.jsonp(result).end();
                        },
                    ).catch(
                    err => handleError(err, res)
                );
            }]
            , auth
        }, __this);

    }

    export function addPagination<T extends baseRepository<any>>(repository: T, pageInfo: PageInfo, res) {
        let result = new PagedData();
        Promise.all([
            repository.count(),
            repository.countAllLike(pageInfo),
            repository.queryAllLike(pageInfo)
        ])
            .then(
                (values) => {
                    pageInfo.total = values[0];
                    pageInfo.count = values[1];
                    result.page = pageInfo;
                    result.data = values[2];
                    res.jsonp(result).end();
                },
            ).catch(
            err => handleError(err, res)
        );
    }

    export function checkAuthentication(req, res, next) {
        let auth = req.get("jwt");
        if (!auth) {
            logger.error("request without token, reject.");
            handleError(defaultException.ERR_nologin, res);
        } else {
            let config = global.config;
            jwt.verify(auth, config.jwtSecKey, (err, decoded) => {
                if (err) {
                    logger.error(err.message);
                    handleError(new serviceException("ERR_relogin", 401, err.message), res);
                } else {
                    logger.silly("user[" + JSON.stringify(decoded) + "] check token success");
                    next();
                }
            });
        }
    }

    /**
     * get user from request sync
     * @param req http request
     * @returns {T} user information
     */
    export function getAuthUserSync(req): any {
        return encryption.getAuthenticationSync(req);
    }

    export function handleError(error: any | serviceException, res) {
        if (error instanceof serviceException) {
            res.status(error.statusCode).jsonp(error);
            logger.error(`server error:${JSON.stringify(error)}`);
        } else {
            logger.error(`unknown server error:${error ? error.stack : error}`);
            res.status(500).jsonp({
                "message": "Server error",
                "statusCode": 500,
                "error": error ? error.stack : error
            });
        }
    }
}
