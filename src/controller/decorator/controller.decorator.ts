/**
 * Created by enixjin on 2/11/18.
 */

import * as express from "express";
import * as multer from "multer";
import "reflect-metadata";
import {baseDomainObject, baseRepository,} from "../..";
import {HttpUtils} from "../httpUtils";
import {IController, ICRUDController, IFileController, requestMappingMetadataKey, RESTAPI} from "./types";

export function Controller(_baseUrl: string, auth = true): any {
    return function <T extends { new(...args: any[]): {} }>(constructor: T) {
        return class extends constructor implements IController {
            baseUrl = _baseUrl;
            router = express.Router();
            getRouter = () => {
                let requestMapping = <RESTAPI[]>Reflect.getMetadata(requestMappingMetadataKey, this);
                if (!requestMapping) {
                    requestMapping = [];
                }
                requestMapping.map(r => {
                    r.url = this.baseUrl + r.url;
                    //inject authentication if necessary
                    if (r.auth === undefined) {
                        r.auth = auth;
                    }
                    if (r.auth) {
                        r.callbacks = [HttpUtils.checkAuthentication, ...r.callbacks];
                    }
                    switch (r.method) {
                        case "POST": {
                            HttpUtils.Post(r, this);
                            break;
                        }
                        case "GET": {
                            HttpUtils.Get(r, this);
                            break;
                        }
                        case "PUT": {
                            HttpUtils.Put(r, this);
                            break;
                        }
                        case "DELETE": {
                            HttpUtils.Delete(r, this);
                            break;
                        }
                        case "PATCH": {
                            HttpUtils.Patch(r, this);
                            break;
                        }
                        case "OPTIONS": {
                            HttpUtils.Options(r, this);
                            break;
                        }
                        case "ALL": {
                            HttpUtils.All(r, this);
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                });
                return this.router;
            }
        }
    }

}

export function CRUDController<T extends baseDomainObject>(entityClass: { new(...args: any[]): T }, auth = true): any {
    let entity = new entityClass();
    let _baseUrl = entity.getRESTUrl();
    return function (constructor) {
        return class extends constructor implements ICRUDController<T> {
            baseUrl = _baseUrl;
            router = express.Router();

            repository: baseRepository<T>;

            getRouter = () => {
                let requestMapping = <RESTAPI[]>Reflect.getMetadata(requestMappingMetadataKey, this);
                if (!requestMapping) {
                    requestMapping = [];
                }
                requestMapping.map(r => {
                    r.url = this.baseUrl + r.url;
                    //inject authentication if necessary
                    if (r.auth === undefined) {
                        r.auth = auth;
                    }
                    if (r.auth) {
                        r.callbacks = [HttpUtils.checkAuthentication, ...r.callbacks];
                    }
                    switch (r.method) {
                        case "POST": {
                            HttpUtils.Post(r, this);
                            break;
                        }
                        case "GET": {
                            HttpUtils.Get(r, this);
                            break;
                        }
                        case "PUT": {
                            HttpUtils.Put(r, this);
                            break;
                        }
                        case "DELETE": {
                            HttpUtils.Delete(r, this);
                            break;
                        }
                        case "PATCH": {
                            HttpUtils.Patch(r, this);
                            break;
                        }
                        case "OPTIONS": {
                            HttpUtils.Options(r, this);
                            break;
                        }
                        case "ALL": {
                            HttpUtils.All(r, this);
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                });
                HttpUtils.createCRUD(this.baseUrl, this, auth);
                return this.router;
            }
        }
    }

}

export function FileUploadController(options: {
    baseUrl?: string,
    uploadFolder?: string,
    urlSingle?: string,
    nameSingle?: string,
    urlMulti?: string,
    nameMulti?: string
}): any {
    return function <T extends { new(...args: any[]): {} }>(constructor: T) {
        options = Object.assign({
            baseUrl: "/upload",
            uploadFolder: "/sandbox/upload",
            urlSingle: "/single",
            nameSingle: "file",
            urlMulti: "/multiple",
            nameMulti: "files"
        }, options);
        return class extends constructor implements IFileController {
            baseUrl = options.baseUrl;
            router = express.Router();
            requestMapping;

            upload = multer({
                storage: multer.diskStorage({
                    destination: options.uploadFolder,
                    filename: function (req, file, cb) {
                        cb(null, file.originalname.split('.')[0] + '.' + Date.now() + '.' + file.originalname.split('.')[1]);
                    }
                })
            });

            getRouter = () => {
                if (!this.requestMapping) {
                    this.requestMapping = [];
                }
                this.router.post(this.baseUrl + options.urlSingle, this.upload.single(options.nameSingle), (req, res, next) => {
                    let file = req.file;
                    if (file) {
                        res.jsonp({success: true, filename: file.filename});
                    } else {
                        res.status(400).jsonp({message: `upload failed, file[${options.nameSingle}] not found in request!`});
                    }
                });
                this.router.post(this.baseUrl + options.urlMulti, this.upload.array(options.nameMulti), (req, res, next) => {
                    let files = JSON.parse(JSON.stringify(req.files));
                    let names = [];
                    for (let file of files) {
                        names.push(file.filename);
                    }
                    if (files) {
                        res.jsonp({success: true, filenames: names});
                    } else {
                        res.status(400).jsonp({message: `upload failed, file[${options.nameMulti}] not found in request!`});
                    }
                });
                return this.router;
            }
        }
    }

}
