/**
 * Created by enixjin on 2/11/18.
 */

import * as express from "express";
import * as multer from "multer";
import {baseDomainObject, baseRepository} from "../../domain";
import {RequestHandler} from "express-serve-static-core";

export type httpCallback = (req, res, next) => Promise<any> | void;

export interface IController {
    router: express.Router;
    baseUrl: string;

    getRouter(): express.Router;
}

export interface ICRUDController<T extends baseDomainObject> extends IController {
    repository: baseRepository<T>;
}

export interface IFileController extends IController {
    upload: multer.Instance;
}

export interface RESTAPI {
    url: string;
    method?: HTTPRequestMethod;
    callbacks?: RequestHandler[];
    auth?: boolean;
}

export type HTTPRequestMethod = "POST" | "GET" | "PUT" | "DELETE" | "PATCH" | "OPTIONS" | "ALL"

export const requestMappingMetadataKey = Symbol("requestMappingMetadataKey");
