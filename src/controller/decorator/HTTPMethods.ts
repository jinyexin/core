/**
 * Created by enixjin on 2/22/18.
 */
import "reflect-metadata";
import {httpCallback, HTTPRequestMethod, requestMappingMetadataKey, RESTAPI} from "./types";
import {HttpUtils} from "../httpUtils";


export function Get(options: RESTAPI | string) {
    return generateRequestMapping(options, "GET");
}

export function Post(options: RESTAPI | string) {
    return generateRequestMapping(options, "POST");
}

export function Put(options: RESTAPI | string) {
    return generateRequestMapping(options, "PUT");
}

export function Delete(options: RESTAPI | string) {
    return generateRequestMapping(options, "DELETE");
}

export function Options(options: RESTAPI | string) {
    return generateRequestMapping(options, "OPTIONS");
}

export function All(options: RESTAPI | string) {
    return generateRequestMapping(options, "ALL");
}

function generateRequestMapping(options: RESTAPI | string, method: HTTPRequestMethod) {
    let _option: RESTAPI = {method: method, url: "/", callbacks: []};
    if (typeof options === "string") {
        options = {url: options};
    }
    _option = Object.assign(_option, options);
    return (target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<httpCallback>) => {
        // save a reference to the original method this way we keep the values currently in the
        // descriptor and don't overwrite what another decorator might have done to the descriptor.
        if (descriptor === undefined) {
            descriptor = Object.getOwnPropertyDescriptor(target, propertyKey);
        }
        if (_option.auth) {
            _option.callbacks = [HttpUtils.checkAuthentication, ..._option.callbacks];
        }

        let originalMethod = descriptor.value;
        // have to use function here, ()=>{} will have this bind issue
        _option.callbacks = [..._option.callbacks, function (req, res, next) {
            let promise = originalMethod.call(this, req, res, next);
            if (promise) {
                return promise.then(
                    result => {
                        if (result) {
                            res.status(200).jsonp(result);
                        }
                        // nothing return, you should handle response yourself!!!
                    },
                ).catch(
                    err => HttpUtils.handleError(err, res)
                );
            }
        }];

        let requestMapping = Reflect.getOwnMetadata(requestMappingMetadataKey, target);
        if (!requestMapping) {
            requestMapping = [];
        }
        requestMapping.push(_option);
        Reflect.defineMetadata(requestMappingMetadataKey, requestMapping, target);
    }
}

