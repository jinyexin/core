/**
 * Created by enixjin on 4/10/17.
 */

import {Request} from "express";
import {baseUser} from "../../domain/entity/baseUser";
import {encryption} from "../../util/encryption";
import {accessControl} from "./accessControl";
import {AccessRights, AccessRule} from "./AccessRule";
import {logger} from "../../util/logger";

export abstract class abstractMethodAccessControl extends accessControl {
    // must have rule.checker:(request,user)=>boolean
    abstract getRules(): AccessRule[];

    validate(request: Request): boolean {
        return this.doValidate(request, encryption.getAuthenticationSync(request))
    }

    public doValidate(request: Request, user: baseUser): boolean {
        for (const rule of this.getRules()) {
            if (this.requestMatches(rule, request)
                && (!user
                    || (rule.right === AccessRights.allowed && !rule.checker(request, user))
                    || (rule.right === AccessRights.denied && rule.checker(request, user))
                )
            ) {
                if (user) {
                    logger.warn(`user[${user.id}] is rejected for request:${request.originalUrl}`);
                } else {
                    logger.warn(`[without jwt] is rejected for request:${request.originalUrl}`);
                }
                return false;
            }
        }
        return true;
    }

}