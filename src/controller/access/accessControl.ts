/**
 * Created by enixjin on 4/7/17.
 */
import {Request} from "express";
import {defaultException} from "../../exception/serviceException";
import {AccessRule} from "./AccessRule";

export abstract class accessControl {
    abstract validate(request: Request): boolean;

    public asExpressMiddleware() {
        return (req, res, next) => {
            if (this.validate(req)) {
                next();
            } else {
                let err = defaultException.ERR_403;
                res.status(err.statusCode);
                res.jsonp(err);
            }
        }
    }


    protected requestMatches(rule: AccessRule, request: Request): boolean {
        return rule.url.test(request.path)
            && (rule.method === "*" || rule.method === request.method);
    }
}