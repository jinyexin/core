/**
 * Created by enixjin on 4/6/17.
 * abstract Access rule module.
 */
export class AccessRule {
    url: RegExp;
    method: "GET" | "POST" | "PUT" | "DELETE" | "*";
    right: AccessRights;
    checker: any | "all";

    /**
     * constructor for create a new access rule
     * @param _url RegExp of the URL
     * @param _checker access checker could be any type: e.g. a string for role based.
     * @param _right access rights @see AccessRights
     * @param _method HTTP method
     */
    constructor(_url: RegExp, _checker: any | "all" = "all", _right: AccessRights = AccessRights.denied, _method: "GET"
        | "POST"
        | "PUT"
        | "DELETE"
        | "*" = "*") {
        this.url = _url;
        this.right = _right;
        this.checker = _checker;
        this.method = _method;
    }
}

export enum AccessRights {
    allowed,//0
    denied,//1
}
