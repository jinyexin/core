/**
 * Created by enixjin on 4/6/17.
 */

import {Request} from "express";
import {baseUser} from "../../domain/entity/baseUser";
import {encryption} from "../../util/encryption";
import {accessControl} from "./accessControl";
import {AccessRights, AccessRule} from "./AccessRule";
import {logger} from "../../util/logger";


export abstract class abstractRoleAccessControl extends accessControl {
    abstract getRules(): AccessRule[];

    validate(request: Request): boolean {
        return this.doValidate(request, encryption.getAuthenticationSync(request))
    }

    public doValidate(request: Request, user: baseUser): boolean {
        for (const rule of this.getRules()) {
            if (this.requestMatches(rule, request)
                && (!user
                    || (rule.right === AccessRights.allowed && !this.rolesContains(rule.checker, [user.role]))
                    || (rule.right === AccessRights.denied && this.rolesContains(rule.checker, [user.role]))
                )
            ) {
                if (user) {
                    logger.warn(`user[${user.id}] is rejected for request:${request.originalUrl}`);
                } else {
                    logger.warn(`[without jwt] is rejected for request:${request.originalUrl}`);
                }
                return false;
            }
        }
        return true;
    }

    private rolesContains(checker: number[] | "all", current: number[]): boolean {
        if (checker === "all") {
            return true;
        } else {
            return checker.reduce((prev, curr) => {
                return prev || current.indexOf(curr) >= 0;
            }, false)
        }
    }
}
