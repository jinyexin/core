/**
 * Created by enixjin on 12/13/16.
 */
import {logger} from "./util";

//initial DI from very beginning
global.dependencyInjectionContainer = new Map();

logger.info(`@jinyexin/core version ${require("../package.json").version}`);

export * from "./controller";
export * from "./db";
export * from "./domain";
export * from "./exception";
export * from "./util";
export * from "./service";
export * from "./di";
export * from "./socket";
export * from "./app";
