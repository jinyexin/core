/**
 * Created by enixjin on 2/20/2019.
 */
import {logger} from "./logger";

const http = require('http');

export namespace httpTools {
    export function sendHttp(hostname: string, port: number, path: string, method: string, postData: string, contentType = "application/json"): Promise<any> {
        return new Promise((resolve, reject) => {
            const options = {
                hostname,
                port,
                path: encodeURI(path),
                method: method,
                headers: {
                    "Content-Type": contentType
                },
            };
            logger.debug(`sending request to ${options.path}, method: ${options.method}`);
            const req = http.request(options, res => {
                let result = "";
                res.on('data', d => {
                    result += d;
                });
                res.on('end', _ => {
                    try {
                        if (res.statusCode >= 200 && res.statusCode < 300) {
                            resolve(result);
                        } else {
                            let jsonResult = JSON.parse(result);
                            reject(jsonResult);
                        }
                    } catch (e) {
                        logger.error(`fail to parse, statusCode:${res.statusCode} result:${result}`);
                        reject(`fail to parse result`);
                    }
                })
            });

            req.on('error', (e) => {
                logger.error(e);
                reject(e);
            });

            if (postData) {
                logger.debug(`writing body: ${postData}`);
                req.write(postData);
            }

            req.end();
        });
    }

    /**
     * IMPORTANT: this method, as a proxy, will always resolve
     * @param hostname hostname
     * @param port port number
     * @param path full path with parameters
     * @param method POST,GET etc
     * @param body body string if any
     * @param contentType default is application/json
     */
    export function proxyHttp(hostname: string, port: number, path: string, method: string, body: string, contentType = "application/json"): Promise<any> {
        return new Promise((resolve, reject) => {
            const options = {
                hostname,
                port,
                path: encodeURI(path),
                method: method,
                headers: {
                    "Content-Type": contentType
                },
            };
            logger.silly(`proxy request ${options.path}, method: ${options.method}`);
            const req = http.request(options, res => {
                let result = "";
                res.on('data', d => {
                    result += d;
                });
                res.on('end', _ => {
                    try {
                        res.body = JSON.parse(result);
                        resolve(res);
                    } catch (e) {
                        logger.error(`fail to proxy, statusCode:${res.statusCode} result:${result}`);
                        resolve({statusCode: 500, body: {message: "fail to proxy"}});
                    }
                })
            });

            req.on('error', (e) => {
                logger.error(e);
                resolve({statusCode: 500, body: {message: "fail to proxy"}});
            });

            if (body) {
                logger.debug(`writing body: ${body}`);
                req.write(body);
            }

            req.end();
        });
    }
}
