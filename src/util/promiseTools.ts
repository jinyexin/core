/**
 * Created by enixjin on 10/31/18.
 */

export namespace promiseTools {

    /**
     * map array to promises which will be executed sequential
     * @param array source array
     * @param promise function map array value to promise
     */
    export function mapSeries(array: any[], promise: (value) => Promise<any>): Promise<any> {
        return array.reduce((p, c) => p.then(() => promise(c)), Promise.resolve());
    }

    /**
     * map array to promises which will be executed parallel
     * @param array
     * @param promise
     */
    export function mapParallel(array: any[], promise: (value) => Promise<any>): Promise<any> {
        return Promise.all(array.map(v => promise(v)));
    }

    /**
     *
     * @param {(resolve, reject) => any} cb callback to create a promise
     * @param {number} timeout timeout of this promise
     * @param {() => any} onTimeout optional on timeout callback
     * @param {string} timeOutMessage optional timeout message
     * @returns {Promise<any>} a promise with timeout
     */
    export function createPromiseWithTimeout(cb: (resolve, reject) => any, timeout: number, onTimeout?: () => any, timeOutMessage = "timeout"): Promise<any> {
        return new Promise((resolve, reject) => {
            cb(resolve, reject);
            setTimeout(() => {
                if (onTimeout) {
                    onTimeout();
                }
                reject(timeOutMessage);
            }, timeout);
        });
    }

    export function createPromiseWithRetry(promise: Promise<any>, retries = 3, defaultError = null) {
        if (retries === 0) {
            return Promise.reject(defaultError)
        }
        return promise.catch(err => createPromiseWithRetry(promise, (retries - 1), err));
    }
}

export class ChainPromise {
    static from = (v: Promise<any>) => new ChainPromise(() => v);

    constructor(public f: (...args) => Promise<any>) {
    }

    map(g): ChainPromise {
        return new ChainPromise(x => Promise.resolve(this.f(x)).then(v => g(v)));
    }

    flatMap(g): ChainPromise {
        return new ChainPromise(x => Promise.resolve(this.f(x))).map(g);
    }

    subscribe(onSuccess?: Function, onFail?: Function): void {
        Promise.resolve(this.f()).then(x => onSuccess(x)).catch(e => onFail(e));
    }
}
