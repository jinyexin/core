/**
 * Created by enixjin on 5/18/16.
 */

import {Subject} from "rxjs";
import {RESTAPI} from "../controller/decorator";
import {logger} from "./logger";

export class apiService {
    apiSteam = new Subject<RESTAPI>();

    private static _instance: apiService = new apiService();

    public static getInstance(): apiService {
        return apiService._instance;
    }

    constructor() {
        logger.silly("init api service");
        if (apiService._instance) {
            throw new Error("Error: Instantiation failed: Use getInstance() instead of new.");
        }
        apiService._instance = this;

        this.apiSteam.subscribe(
            info => {
                logger.debug(`creating api on URL:{${info.method}}[auth:${info.auth}]    ${info.url}`);
            }
        );
    }

    push(info: RESTAPI) {
        this.apiSteam.next(info);
    }

}
