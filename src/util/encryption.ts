/**
 * Created by enixjin on 6/15/16.
 */
import * as jwt from "jsonwebtoken";
import {baseUser} from "../domain/entity/baseUser";
import {defaultException} from "../exception/serviceException";
import {logger} from "./logger";

export namespace encryption {
    let crypto = require('crypto');

    let algorithm_aes128 = 'aes128';
    let algorithm_rc4 = 'rc4';

    export function encryptMd5(s: string): any {
        s = s.toString().toLowerCase().trim();
        let md5 = crypto.createHash('md5');
        md5.update(s);
        return md5.digest('hex');
    }

    export function encryptRC4(content: Buffer | string) {
        let config = global.config;
        return cipher(algorithm_rc4, config.rc4_key, content, config.rc4_iv);
    }

    export function decryptRC4(content: Buffer | string) {
        let config = global.config;
        return decipher(algorithm_rc4, config.rc4_key, content, config.rc4_iv);
    }

    export function encryptAES128(key: Buffer | string, content: Buffer | string) {
        let config = global.config;
        return cipher(algorithm_aes128, key, content, config.aes128_iv);
    }

    export function decryptAES128(key: Buffer | string, content: Buffer | string) {
        let config = global.config;
        return decipher(algorithm_aes128, key, content, config.aes128_iv);
    }

    export function getAuthentication<T extends baseUser>(req): Promise<T> {
        return new Promise((resolve, reject) => {
                let auth = req.get("jwt");
                if (!auth) {
                    reject(defaultException.ERR_nologin);
                } else {
                    let config = global.config;
                    jwt.verify(auth, config.jwtSecKey, (err, decoded) => {
                        if (err) {
                            reject(defaultException.ERR_relogin);
                        } else {
                            logger.silly("user from token:[" + JSON.stringify(decoded) + "]");
                            resolve(decoded);
                        }
                    });
                }
            }
        );
    }

    export function getAuthenticationSync<T extends baseUser>(req): T {
        try {
            let auth = req.get("jwt");
            let config = global.config;
            return jwt.verify(auth, config.jwtSecKey) as T;
        } catch (err) {
            logger.error("decode jwt fail!");
            return null;
        }
    }

    function cipher(algorithm, key: Buffer | string, content: Buffer | string, algorithm_iv: Buffer) {
        key = getBuffer(key);
        content = getBuffer(content);
        let cip = crypto.createCipheriv(algorithm, key, algorithm_iv);
        let encrypted = cip.update(content, 'hex', 'hex');
        encrypted += cip.final('hex');
        return encrypted.toString('hex');
    }

    function decipher(algorithm, key, content, algorithm_iv) {
        key = getBuffer(key);
        content = getBuffer(content);
        let decipher = crypto.createDecipheriv(algorithm, key, algorithm_iv);
        let decrypted = decipher.update(content, 'hex', 'hex');
        decrypted += decipher.final('hex');
        return decrypted.toString('hex');
    }

    function getBuffer(cont: Buffer | string): Buffer {
        if (cont instanceof Buffer) {
            return cont;
        } else {
            return new Buffer(cont, 'hex');
        }
    }

}
