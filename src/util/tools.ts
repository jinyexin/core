/**
 * Created by Simon on 2016/4/7.
 */
let crypto = require('crypto');

export namespace tools {
    const charsetNumber = '1234567890';


    export function isEmptyObject(obj): boolean {
        for (let key in obj) {
            return false;
        }
        return true;
    }

    export function fillToLength(original, length: number): string {
        let shex = original ? original.toString(16) : "";

        if (shex.length > length * 2) {
            throw new Error("Illegal data length in fillToLength(): " + original + ".length > " + length);
        }

        while (shex.length < length * 2) {
            shex = '0' + shex;
        }
        return shex;
    }

    export function getHash(indicator: string): number {
        let config = global.config;
        let dbCount: number = config.distributedDBs.length;
        let last = indicator.slice(-1);
        if (isNaN(parseInt(last[0]))) {
            return 0;
        } else {
            return <any>last[0] % dbCount;
        }
    }

    export function getIpAddress(req) {
        let ipAddress;
        let headers = req.headers;
        let forwardedIpsStr = headers['x-forwarded-for'];
        forwardedIpsStr ? ipAddress = forwardedIpsStr : ipAddress = null;
        if (!ipAddress) {
            ipAddress = req.connection.remoteAddress ||
                req.socket.remoteAddress ||
                req.connection.socket.remoteAddress;
        }
        return ipAddress;
    }

    export function isErrorCountReset(firstTime: any, minute: number) {
        let flag = false;
        let betweenTime = new Date().getTime() - firstTime.getTime();
        if (betweenTime > (1000 * 60 * minute)) {
            flag = true;
        }
        return flag;
    }

    export function generateRandomNumber(length: number) {
        return generateRandom(length, charsetNumber);
    }

    export function delay(time: number): Promise<any> {
        return new Promise<any>(resolve => {
            setTimeout(resolve, time);
        });
    }

    export function until(cond: () => Promise<any>, time: number): Promise<any> {
        return cond().then(
            result => result || delay(time).then(() => until(cond, time))
        );
    }

    export function getSocketIO() {
        return global.dependencyInjectionContainer.get("socket.io");
    }

    function generateRandom(length: number, charset: string): string {
        let result = '';
        while (result.length < length) {
            let bf;
            try {
                bf = crypto.randomBytes(length);
            } catch (e) {
                continue;
            }
            for (let i = 0; i < bf.length; i++) {
                let index = bf.readUInt8(i) % charset.length;
                result += charset.charAt(index);
            }
        }

        return result;
    }
}
