/**
 * Created by enixjin on 10/10/18.
 */
import {format} from "winston";
import * as winston from 'winston';
import * as fs from "fs-extra";

let config = global.config;
let fileFormat = format.combine(
    format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
    }),
    format.prettyPrint(),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
);
let consoleFormat = format.combine(
    format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
    }),
    format.prettyPrint(),
    format.colorize(),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
);
let transports = [];
if (config.logFile) {
    fs.ensureFileSync(config.logFile);
    const files = new winston.transports.File({
        filename: config.logFile,
        format: fileFormat,
        maxsize: 400000,
        maxFiles: 10,
        tailable: true,
        level: config.logLevelFile ? config.logLevelFile : config.logLevel,
    });
    transports.push(files);

}

const console = new winston.transports.Console({
    format: consoleFormat,
    level: config.logLevel,
});
transports.push(console);

export const logger = winston.createLogger({
    transports
});