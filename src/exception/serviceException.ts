/**
 * Created by admin on 4/15/2016.
 */

export type HTTPStatusCode =
    100
    | 101
    | 102
    | 200
    | 201
    | 202
    | 203
    | 204
    | 205
    | 206
    | 207
    | 208
    | 226
    | 300
    | 301
    | 302
    | 304
    | 305
    | 306
    | 307
    | 308
    | 400
    | 401
    | 402
    | 403
    | 404
    | 405
    | 406
    | 407
    | 408
    | 409
    | 410
    | 411
    | 412
    | 413
    | 414
    | 415
    | 416
    | 417
    | 418
    | 421
    | 422
    | 423
    | 424
    | 426
    | 428
    | 429
    | 431
    | 451
    | 500
    | 501
    | 502
    | 504
    | 505
    | 506
    | 507
    | 508
    | 510
    | 511;

export class serviceException {

    constructor(public message: string, public statusCode: HTTPStatusCode, public error?: string) {
    }

}

export const defaultException = {
    ERR_404: new serviceException("Request not found!", 404),
    ERR_500: new serviceException("Server Error!", 500),
    ERR_nologin: new serviceException("Token Missing!", 401),
    ERR_relogin: new serviceException("Token Timeout!", 401),
    WARNING_authFail: new serviceException("Login Fail！", 400),
    ERR_401: new serviceException("UNAUTHORIZED", 401),
    ERR_403: new serviceException("Insufficient access", 403),
    ERR_DB: new serviceException("Database failure!", 500),
    ERR_OpenIDMissing: new serviceException("Fail to find open_id", 500),
    ERR_400: new serviceException("Bad Request", 400),
};