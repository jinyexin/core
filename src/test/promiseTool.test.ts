/**
 * Created by enixjin on 2/1/2019.
 */
import {promiseTools} from "../util";

describe("Promise tool Test", () => {
    it("should create promise with timeout", (done) => {
        promiseTools.createPromiseWithTimeout(
            (resolve, reject) => {
                setTimeout(() => {
                    resolve("resolve");
                }, 100)
            },
            50
        ).then(
            () => {
                done("promise should be timeout after 50ms");
            }
        ).catch(
            reason => {
                done();
            }
        );
    });
});
