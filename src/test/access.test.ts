/**
 * Created by enixjin on 6/3/16.
 */
global.config = require("../../config");
import {assert} from "chai";
import {Request} from "express";
import * as jwt from "jsonwebtoken";
import * as winston from "winston";
import {abstractMethodAccessControl, abstractRoleAccessControl, AccessRights, AccessRule} from "../controller/access";


class testAccessControl extends abstractRoleAccessControl {
    getRules() {
        return [
            // user and teacher can not access /users
            new AccessRule(/^\/users/, [appRole.user, appRole.teacher], AccessRights.denied, "*"),
            // user cannot login
            new AccessRule(/^\/login/, [appRole.user], AccessRights.denied, "POST"),
        ]
    }
}

class testMethodAccessControl extends abstractMethodAccessControl {
    getRules() {
        return [
            // only same user could access user/:selfID
            new AccessRule(/^\/users/, this.notSameUser, AccessRights.denied, "*")
        ];
    }

    notSameUser(request, user) {
        return request.params.id !== user.id;
    }
}

enum appRole {
    user,
    teacher,
    admin
}

function generateGWTToken(user) {
    let config = global.config;
    return jwt.sign({
        id: user.id,
        username: user.username,
        role: user.role
    } as any, config.jwtSecKey, {expiresIn: config.jwtTimeout});
}

describe("Access test", function () {
    before(function () {
        winston.remove(winston.transports.Console);
    });
    describe("Role access test", function () {

        it("should fail when no jwt header with url:/users/1 (GET)", () => {
            let tac = new testAccessControl();
            let req = {
                path: "/users/1", get: (name) => {
                    return null;
                },
                method: "GET"
            };
            assert.isFalse(tac.validate(req as Request));
        });

        it("should pass when no jwt header with no matched url:/another/1 (GET)", () => {
            let tac = new testAccessControl();
            let req = {
                path: "/another/1", get: (name) => {
                    return null;
                },
                method: "GET"
            };
            assert.isTrue(tac.validate(req as Request));
        });

        it("should fail for role:user with url:/users/1 (GET)", () => {
            let tac = new testAccessControl();
            let req = {
                path: "/users/1", get: (name) => {
                    return generateGWTToken({id: 1, username: "enixjin", role: appRole.user});
                },
                method: "GET"
            };
            assert.isFalse(tac.validate(req as Request));
        });

        it("should fail for role:user with url:/users/1 (DELETE)", () => {
            let tac = new testAccessControl();
            let req = {
                path: "/users/1", get: (name) => {
                    return generateGWTToken({id: 1, username: "enixjin", role: appRole.user});
                },
                method: "DELETE"
            };
            assert.isFalse(tac.validate(req as Request));
        });

        it("should pass for role:admin with url:/users/1 (GET)", () => {
            let tac = new testAccessControl();
            let req = {
                path: "/users/1", get: (name) => {
                    return generateGWTToken({id: 1, username: "enixjin", role: appRole.admin});
                },
                method: "GET"
            };
            assert.isTrue(tac.validate(req as Request));
        });

        it("should pass for role:admin with url:/another (GET)", () => {
            let tac = new testAccessControl();
            let req = {
                path: "/another", get: (name) => {
                    return generateGWTToken({id: 1, username: "enixjin", role: appRole.admin});
                },
                method: "GET"
            };
            assert.isTrue(tac.validate(req as Request));
        });

        it("should pass for role:teacher with url:/login (POST)", () => {
            let tac = new testAccessControl();
            let req = {
                path: "/login", get: (name) => {
                    return generateGWTToken({id: 1, username: "enixjin", role: appRole.teacher});
                },
                method: "POST"
            };
            assert.isTrue(tac.validate(req as Request));
        });

        it("should fail for role:user with url:/login (POST)", () => {
            let tac = new testAccessControl();
            let req = {
                path: "/users/1", get: (name) => {
                    return generateGWTToken({id: 1, username: "enixjin", role: appRole.user});
                },
                method: "POST"
            };
            assert.isFalse(tac.validate(req as Request));
        });


    });


    describe("DB access test", function () {

        it("should fail when no jwt header with matched url:/users/1 (GET)", () => {
            let tac = new testMethodAccessControl();
            let req = {
                path: "/users/1", get: (name) => {
                    return null;
                },
                method: "GET"
            };
            assert.isFalse(tac.validate(req as Request));
        });

        it("should pass when no jwt header with no matched url:/another/1 (GET)", () => {
            let tac = new testMethodAccessControl();
            let req = {
                path: "/another/1", get: (name) => {
                    return null;
                },
                method: "GET"
            };
            assert.isTrue(tac.validate(req as Request));
        });

        it("should pass for role:admin with url:/another (GET)", () => {
            let tac = new testMethodAccessControl();
            let req = {
                path: "/another", get: (name) => {
                    return generateGWTToken({id: 1, username: "enixjin", role: appRole.admin});
                },
                method: "GET"
            };
            assert.isTrue(tac.validate(req as Request));
        });

        it("should fail for user{id:2} with url:/users/1 (GET)", () => {
            let tac = new testMethodAccessControl();
            let req = {
                path: "/users/1", get: (name) => {
                    return generateGWTToken({id: 2, username: "enixjin2", role: appRole.user});
                },
                method: "GET",
                params: {
                    id: 1
                }
            };
            assert.isFalse(tac.validate(req as Request));
        });

        it("should pass for user{id:1} with url:/users/1 (PUT)", () => {
            let tac = new testMethodAccessControl();
            let req = {
                path: "/users/1", get: (name) => {
                    return generateGWTToken({id: 2, username: "enixjin2", role: appRole.user});
                },
                method: "PUT",
                params: {
                    id: 1
                }
            };
            assert.isFalse(tac.validate(req as Request));
        });

    });


});
