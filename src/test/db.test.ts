/**
 * Created by enixjin on 6/3/16.
 */
import {initDB} from "../db/baseDB";
import {centralizedMySQL} from "../db/MySQL/centralizedMySQL";
import {distributedMySQL} from "../db/MySQL/distributedMySQL";
import {centralizedPostgreSQL} from "../db/PostgreSQLDB/centralizedPostgreSQL";


describe("DB test", function () {
    before(function () {
        global.config = require("../../config");
        //mysql
        initDB("MySQL");
        //postgre
        initDB("PostgreSQL");
    });
    describe("Basic db test(MySQL)", function () {

        it.skip("should get correct centralized db pool", (done) => {
            centralizedMySQL.getInstance().query(
                "SELECT version()",
                []
            ).then(
                (result) => {
                    done();
                }
            );
        });

        it.skip("should get correct distributed db pool", (done) => {
            distributedMySQL.getInstance().query(
                "SELECT version()",
                [],
                "indicator"
            ).then(
                (result) => {
                    done();
                }
            );
        });

        it.skip("should throw error while not give indicator for distributed db", (done) => {
            distributedMySQL.getInstance().query(
                "SELECT version()",
                []
            ).then(
                () => {
                    done("A query to distributed db should always give indicator");
                },
                () => {
                    done();
                }
            );
        });
    });

    describe("Basic db test(PostgreSQL)", function () {

        it.skip("should get correct centralized db pool", (done) => {
            centralizedPostgreSQL.getInstance().query(
                "SELECT version()",
                []
            ).then(
                (result) => {
                    done();
                }
            );
        });

    });

});
