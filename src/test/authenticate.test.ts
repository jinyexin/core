/**
 * Created by enixjin on 6/3/16.
 */
import {initDB} from "../db/baseDB";
import {serviceFactory} from "../service/serviceFactory";
import {serviceFactoryImpl} from "../service/impl/serviceFactoryImpl";

describe("authenticate test", function () {
    before(function () {
        global.config = require("../../config");
        // winston.remove(winston.transports.Console);
        initDB("MySQL");
    });
    describe("Basic login test(MySQL)", function () {

        it.skip("should success login with right username password", (done) => {
            let factory: serviceFactory = serviceFactoryImpl.getInstance();
            factory.getAuthenticationService().login("enixjin", "111111").then(
                (user) => {
                    if (user) {
                        done();
                    } else {
                        done("login fail");
                    }
                }
            );
        });
    });

});
