/**
 * Created by enixjin on 6/3/16.
 */

import {assert} from 'chai';
import {RESTEntity} from "../domain/decorator";
import {dbType} from "../db";
import {baseDomainObject} from "../domain";

@RESTEntity({
    table: "testTable",
    URL: "testURL",
    db: dbType.centralized
})
class testEntity extends baseDomainObject {

}

describe("Decorator Test", () => {
    let entity = new testEntity();
    it("should update getTableName/getRESTUrl/getDBType", () => {
        assert.equal(entity.getTableName(), "testTable");
        assert.equal(entity.getRESTUrl(), "testURL");
        assert.equal(entity.getDBType(), dbType.centralized);
    });
});